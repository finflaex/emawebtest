﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace EmaWebTest.Hubs
{
    public class DrawHub : Hub
    {
        public Task Draw(int prevX, int prevY, int currentX, int currentY, string color)
        {
            return Clients
                .AllExcept(new List<string> {Context.ConnectionId})
                .SendAsync("draw", prevX, prevY, currentX, currentY, color);
        }
    }
}