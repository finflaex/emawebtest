﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Finflaex.Support.Extensions;
using Microsoft.AspNetCore.SignalR;

namespace EmaWebTest.Hubs
{
    public class TeleMechanicHub : Hub
    {
        private bool flag = false;

        public override async Task OnConnectedAsync()
        {
            var random = new Random();
            flag = true;
            await Task.Run(async () =>
            {
                while (flag)
                {
                    var value = new
                    {
                        a = random.Next(20, 40),
                        b = random.Next(0, 25),
                        c = random.Next(180, 220)
                    };
                    await Clients.Client(Context.ConnectionId).SendAsync("send",value.ToJson());
                    await Task.Delay(500);
                }
            });
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            flag = false;
        }

        public async Task Send(string message, string user)
        {
            await Clients.All.SendAsync("Send",
                $"{DateTime.Now:HH:mm:ss} <b>{user.RetIfNullOrWhiteSpace("unknown")}:</b> {message}");
        }
    }
}
