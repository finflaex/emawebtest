﻿using System;
using System.Threading.Tasks;
using Finflaex.Support.Extensions;
using Microsoft.AspNetCore.SignalR;

namespace EmaWebTest.Hubs
{
    public class ChatHub : Hub
    {
        public async Task Send(string message, string user)
        {
            await Clients.All.SendAsync("Send",
                $"{DateTime.Now:HH:mm:ss} <b>{user.RetIfNullOrWhiteSpace("unknown")}:</b> {message}");
        }
    }
}