﻿using System;

namespace EmaWebTest.Models
{
    public class GuidValue
    {
        public Guid Guid { get; set; }
        public string Value { get; set; }
    }
}