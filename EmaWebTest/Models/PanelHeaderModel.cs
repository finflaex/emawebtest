﻿namespace EmaWebTest.Models
{
    public class PanelHeaderModel
    {
        public string Caption { get; set; }
        public bool Collapse { get; set; }
        public bool Reload { get; set; }
        public bool Close { get; set; }
    }
}