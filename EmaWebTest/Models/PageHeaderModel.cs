﻿using System.Collections.Generic;

namespace EmaWebTest.Models
{
    public class PageHeaderModel
    {
        public PageHeaderModel()
        {
            Buttons = new List<ActionLinkModel>();
            Breadcrumb = new List<ActionLinkModel>();
            Menu = new List<ActionLinkModel>();
        }

        public string Caption { get; set; }
        public string Description { get; set; }

        public IEnumerable<ActionLinkModel> Buttons { get; set; }
        public IEnumerable<ActionLinkModel> Breadcrumb { get; set; }
        public IEnumerable<ActionLinkModel> Menu { get; set; }
        public object Html { get; set; }
    }

    public class ActionLinkModel
    {
        public ActionLinkModel()
        {
            Subitem = new List<ActionLinkModel>();
        }

        public string Url { get; set; }
        public string Caption { get; set; }
        public string Icon { get; set; }
        public IEnumerable<ActionLinkModel> Subitem { get; set; }
        public string OnClick { get; set; }
    }
}