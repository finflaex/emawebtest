﻿using System;
using Finflaex.Support.Models;

namespace EmaWebTest.Models
{
    public class AuthorizationModel :
        FxaAuthorizationModel
    {
        public AuthorizationModel()
        {
            SessionId = Guid.NewGuid();
        }

        public bool ToggleMain { get; set; }
        public bool ToggleSecondary { get; set; }
        public bool ToggleOpposite { get; set; }
    }
}