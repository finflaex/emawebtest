﻿using System.Diagnostics;
using Finflaex.Support.Extensions;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace EmaWebTest.API.Controllers
{
    [Route("api/[controller]/[action]/{format}")]
    [EnableCors("AllowAll")]
    [FormatFilter]
    public class Test : ControllerBase
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly IMemoryCache _cache;
        private readonly IConfiguration _config;

        public Test(
            IHttpContextAccessor accessor,
            IMemoryCache cache,
            IConfiguration config
        )
        {
            _accessor = accessor;
            _cache = cache;
            _config = config;
        }

        // http://localhost:8000/api/test/get/xml/1
        // http://localhost:8000/api/test/get/json/1
        [HttpGet("{id}")]
        public model Get(int id)
        {
            return new model
            {
                id = id,
                value = $"test_value {id}"
            };
        }

        [HttpPost]
        public void Post([FromBody] model model)
        {
            Debug.WriteLine(model.ToJson().Prepend($"{nameof(Post)} > "));
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody] model model)
        {
            model.id = id;
            Debug.WriteLine(model.ToJson().Prepend($"{nameof(Put)} > ").Append($" by id {id}"));
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Debug.WriteLine($"delte id = {id}");
        }

        public class model
        {
            public int id { get; set; }
            public string value { get; set; }
        }
    }
}