﻿using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace EmaWebTest.Support.Telerik
{
    public class fxcLimitlessTableContext
        : ContextMenuBuilder
    {
        private string _StringName;

        public fxcLimitlessTableContext(ContextMenu component)
            : base(component)
        {
            Filter(".k-selectable [role = 'row']");
            Orientation(ContextMenuOrientation.Vertical);
            Animation(animation =>
            {
                animation.Open(open =>
                {
                    open.Fade(FadeDirection.In);
                    open.Duration(200);
                });
            });
        }

        public string StringName
        {
            get => _StringName;
            set => Name(_StringName = value);
        }
    }

    public static class fxrLimitlessTableContext
    {
        public static fxcLimitlessTableContext ToLimitlessTableContext(
            this ContextMenuBuilder @this)
        {
            var result = new fxcLimitlessTableContext(@this.ToComponent());

            return result;
        }

        public static ContextMenuItemBuilder Function(
            this ContextMenuItemFactory @this,
            string Caption,
            string JsFunctionName)
        {
            return @this.Add().Text(Caption).HtmlAttributes(new {onclick = $"{JsFunctionName}()"});
        }

        public static ContextMenuItemBuilder GridDataUrl(
            this ContextMenuItemFactory @this,
            string Caption,
            string GridName,
            string StringUrl,
            string ValueName = "ID")
        {
            return @this.Add().Text(Caption).HtmlAttributes(new
            {
                onclick = $"GridContextLink('{GridName}','{StringUrl}','{ValueName}')"
            });
        }
    }
}