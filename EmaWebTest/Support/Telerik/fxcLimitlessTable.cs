﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq.Expressions;
using System.Threading;
using Finflaex.Support.Extensions;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace EmaWebTest.Support.Telerik
{
    public class fxcLimitlessTable<T>
        : GridBuilder<T>
        where T : class
    {
        private string _StringName;

        public fxcLimitlessTable(Grid<T> component)
            : base(component)
        {
            var culture = new CultureInfo("ru-RU");
            culture.NumberFormat.NumberDecimalSeparator = ",";
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            //Sortable();
            Sortable(sortable => sortable
                .AllowUnsort(true)
                .SortMode(GridSortMode.MultipleColumn)
                .ShowIndexes(true));

            //Filterable(ftb => { ftb.Mode(GridFilterMode.Row); });
            Filterable(ftb => ftb.Mode(GridFilterMode.Menu));
            ColumnMenu();

//            ToolBar(tools => tools.Pdf().Text("Вывод в PDF"));
//            Pdf(pdf =>
//            {
//                pdf
//                    .AllPages()
//                    .AvoidLinks()
//                    .PaperSize("A4")
//                    .Margin("2cm", "1cm", "1cm", "1cm")
//                    .Landscape()
//                    .RepeatHeaders()
//                    .TemplateId("page-template")
//                    .FileName("Export.pdf")
//                    .ProxyURL("/base/export/save")
//                    ;
//            });

            ToolBar(tools => tools.Excel().Text("Вывод в EXCEL"));
            Excel(excel =>
            {
                excel
                    .FileName("Export.xlsx")
                    .Filterable(true)
                    .ProxyURL("/base/export/save")
                    ;
            });

            Scrollable(sc => { sc.Endless(false); });

            Selectable(v =>
            {
                v.Type(GridSelectionType.Row);
                v.Mode(GridSelectionMode.Single);
            });
            Pageable(page =>
            {
                page.Numeric(true);
                page.PreviousNext(true);
                page.Info(true);
                page.Refresh(true);
                page.PageSizes(new[] {100, 1000, 10000});
            });
            Events(e =>
            {
                e.DataBound("onEditDataBound");
                e.BeforeEdit("onEditDataBound");
                e.Cancel("onEditDataBound");
                e.Change("onEditDataBound");
            });
            Resizable(resize => resize.Columns(true));
            Reorderable(reorder => reorder.Columns(true));
        }

        public string StringName
        {
            get => _StringName;
            set => Name(_StringName = value);
        }

        public fxcLimitlessTable<T> DataSourceAjaxDefault(
            string controller,
            string area,
            Action<AjaxDataSourceBuilder<T>> act)
        {
            controller = controller.ToLower().RemoveEnd("controller");
            var url = area == null ? $"/{controller}" : $"/{area}/{controller}";

            DataSource(dataSource =>
            {
                act(dataSource.Ajax()
                    .PageSize(100)
                    .Events(events => events.Error("errorHandler"))
                    .Read(v => v.Url($"{url}/Read/"))
                    .Create(v => v.Url($"{url}/Create/"))
                    .Update(v => v.Url($"{url}/Update/"))
                    .Destroy(v => v.Url($"{url}/Destroy/")));
            });

            return this;
        }

        public fxcLimitlessTable<T> DataSourceWebApiDefault(
            string url,
            Action<AjaxDataSourceBuilder<T>> act)
        {
            DataSource(dataSource =>
            {
                act(dataSource.WebApi()
                    .PageSize(100)
                    .Events(events => events.Error("errorHandler"))
                    .Read(v => v.Url($"{url}/Read/"))
                    .Create(v => v.Url($"{url}/Create/"))
                    .Update(v => v.Url($"{url}/Update/"))
                    .Destroy(v => v.Url($"{url}/Destroy/")));
            });

            return this;
        }
    }

    public static class fxrLimitlessTable
    {
        public static fxcLimitlessTable<T> ToLimitlessTable<T>(
            this GridBuilder<T> @this)
            where T : class
        {
            var result = new fxcLimitlessTable<T>(@this.ToComponent());

            return result;
        }

        public static GridBoundColumnBuilder<T> AsUrl<T>(
            this GridBoundColumnBuilder<T> @this,
            string Caption,
            string Url)
            where T : class
        {
            return @this.ClientTemplate($"<a href='{Url}'>{Caption}</a>");
        }

        public static GridBoundColumnBuilder<T> AsDownload<T>(
            this GridBoundColumnBuilder<T> @this,
            string Caption,
            string FileName,
            string Url)
            where T : class
        {
            return @this.ClientTemplate($"<a href='{Url}' download='{FileName}'>{Caption}</a>");
        }

        public static GridBoundColumnBuilder<T> AsDate<T>(
            this GridBoundColumnBuilder<T> @this,
            string Name)
            where T : class
        {
            return @this
                .EditorTemplateName("Date")
                .ClientTemplate($"#= kendo.toString({Name}, 'dd.MM.yyyy') #");
        }

        public static GridBoundColumnBuilder<T> AsDateTime<T>(
            this GridBoundColumnBuilder<T> @this,
            string Name)
            where T : class
        {
            return @this
                .EditorTemplateName("DateTime")
                .ClientTemplate($"#= kendo.toString({Name}, 'dd.MM.yyyy HH:mm') #");
        }

        public static GridBoundColumnBuilder<T> AsDateStart<T>(
            this GridBoundColumnBuilder<T> @this,
            string Name)
            where T : class
        {
            return @this
                .EditorTemplateName("Date")
                .ClientTemplate($"#= kendo.toString({Name}, 'dd.MM.yyyy') #")
                .Filterable(v => v.Cell(c => c.ShowOperators(false).Operator("lte")));
        }

        public static GridBoundColumnBuilder<T> AsDateEnd<T>(
            this GridBoundColumnBuilder<T> @this,
            string Name)
            where T : class
        {
            return @this
                .EditorTemplateName("Date")
                .ClientTemplate($"#= kendo.toString({Name}, 'dd.MM.yyyy') #")
                .Filterable(v => v.Cell(c => c.ShowOperators(false).Operator("gte")));
        }

        public static GridBoundColumnBuilder<T> AsDateTimeStart<T>(
            this GridBoundColumnBuilder<T> @this,
            string Name)
            where T : class
        {
            return @this
                .EditorTemplateName("DateTime")
                .ClientTemplate($"#= kendo.toString({Name}, 'dd.MM.yyyy HH:mm') #")
                .Filterable(v => v.Cell(c => c.ShowOperators(false).Operator("lte")));
        }

        public static GridBoundColumnBuilder<T> AsDateTimeEnd<T>(
            this GridBoundColumnBuilder<T> @this,
            string Name)
            where T : class
        {
            return @this
                .EditorTemplateName("DateTime")
                .ClientTemplate($"#= kendo.toString({Name}, 'dd.MM.yyyy HH:mm') #")
                .Filterable(v => v.Cell(c => c.ShowOperators(false).Operator("gte")));
        }

        public static GridBoundColumnBuilder<T> FilterContains<T>(
            this GridBoundColumnBuilder<T> @this)
            where T : class
        {
            return @this.Filterable(v => v.Cell(c => c.ShowOperators(false).Operator("contains")));
        }

        public static GridBoundColumnBuilder<T> FilterEquals<T>(
            this GridBoundColumnBuilder<T> @this)
            where T : class
        {
            return @this.Filterable(v => v.Cell(c => c.ShowOperators(false).Operator("equals")));
        }

        public static GridBoundColumnBuilder<T> FilterButtonNone<T>(
            this GridBoundColumnBuilder<T> @this)
            where T : class
        {
            return @this.Filterable(v => v.Cell(c => c.ShowOperators(false)));
        }

        public static GridActionColumnBuilder CommandEdit<T>(
            this GridColumnFactory<T> @this,
            bool edit = true,
            bool delete = true)
            where T : class
        {
            return @this.Command(command =>
                {
                    if (edit) command.Edit();
                    if (delete) command.Destroy();
                })
                .Width(88);
        }

        public static GridBoundColumnBuilder<T> ComboBox<T, K>(
            this GridColumnFactory<T> @this,
            Expression<Func<T, K>> expression,
            IEnumerable list)
            where T : class
        {
            return @this
                .ForeignKey(expression, list, "ID", "Caption")
                .EditorTemplateName("ComboBox");
        }

        public static GridBoundColumnBuilder<T> DropDownList<T, K>(
            this GridColumnFactory<T> @this,
            Expression<Func<T, K>> expression,
            IEnumerable list)
            where T : class
        {
            return @this
                .ForeignKey(expression, list, "ID", "Caption")
                .EditorTemplateName("DropDownList");
        }
    }
}