﻿using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EmaWebTest.Support.Telerik
{
    public class fxcLimitless<T>
    {
        public fxcLimitless(IHtmlHelper<T> helper)
        {
            Helper = helper;
        }

        public IHtmlHelper<T> Helper { get; set; }

        public fxcLimitlessTable<TModel> Table<TModel>(string name)
            where TModel : class
        {
            var result = Helper.Kendo().Grid<TModel>().ToLimitlessTable();
            if (name != null) result.StringName = name;
            return result;
        }

        public fxcLimitlessTableContext TableContext(string name)
        {
            var result = Helper.Kendo().ContextMenu().ToLimitlessTableContext();
            if (name != null) result.StringName = name;
            return result;
        }

        public fxcLimitlessTreeList<TModel> TreeList<TModel>(string name = null)
            where TModel : class
        {
            var result = Helper.Kendo().TreeList<TModel>().ToLimitlessTreeList();
            if (name != null) result.StringName = name;
            return result;
        }
    }

    public static class fxrLimitlessHelper
    {
        public static fxcLimitless<T> Limitless<T>(
            this IHtmlHelper<T> @this)
        {
            return new fxcLimitless<T>(@this);
        }
    }
}