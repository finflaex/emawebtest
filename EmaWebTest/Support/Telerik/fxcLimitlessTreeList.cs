﻿using System;
using System.Globalization;
using System.Linq.Expressions;
using System.Threading;
using Finflaex.Support.Extensions;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;

namespace EmaWebTest.Support.Telerik
{
    public class fxcLimitlessTreeList<T>
        : TreeListBuilder<T>
        where T : class
    {
        private string _StringName;

        public fxcLimitlessTreeList(TreeList<T> component)
            : base(component)
        {
            var culture = new CultureInfo("ru-RU");
            culture.NumberFormat.NumberDecimalSeparator = ",";
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            //Sortable();
            Sortable(sortable => sortable
                    .AllowUnsort(true)
                //.SortMode(GridSortMode.MultipleColumn)
                //.ShowIndexes(true)
            );

            //Filterable(ftb => { ftb.Mode(GridFilterMode.Row); });
            //Filterable(ftb => ftb.Mode(GridFilterMode.Menu));
            Filterable(true);
            ColumnMenu();

//            Toolbar(tools => tools.Pdf().Text("Вывод в PDF"));
//            Pdf(pdf =>
//            {
//                pdf
//                    // .AllPages()
//                    .AvoidLinks()
//                    .PaperSize("A4")
//                    .Margin("2cm", "1cm", "1cm", "1cm")
//                    .Landscape()
//                    //.RepeatHeaders()
//                    //.TemplateId("page-template")
//                    .FileName("Export.pdf")
//                    .ProxyURL("/base/export/save")
//                    ;
//            });

            Toolbar(tools => tools.Excel().Text("Вывод в EXCEL"));
            Excel(excel =>
            {
                excel
                    .FileName("Export.xlsx")
                    .Filterable(true)
                    .ProxyURL("/base/export/save")
                    ;
            });

            Selectable(select => select.Mode(TreeListSelectionMode.Single).Type(TreeListSelectionType.Row));

            Events(e =>
            {
                e.DataBound("onEditDataBound");
                e.Edit("onEditDataBound");
                e.Cancel("onEditDataBound");
                e.Change("onEditDataBound");
                e.DragStart("dragStart");
                e.DragEnd("dragEnd");
            });


            Resizable(true);
            Reorderable(true);
        }

        public string StringName
        {
            get => _StringName;
            set => Name(_StringName = value);
        }

        public fxcLimitlessTreeList<T> DataSourceAjaxDefault(
            string controller,
            string area,
            Action<TreeListAjaxDataSourceBuilder<T>> act)
        {
            controller = controller.ToLower().RemoveEnd("controller");
            var url = area == null ? $"/{controller}" : $"/{area}/{controller}";

            DataSource(dataSource =>
            {
                act(dataSource
                    .PageSize(100)
                    .Events(events => events.Error("errorHandler"))
                    .Read(v => v.Url($"{url}/Read/"))
                    .Create(v => v.Url($"{url}/Create/"))
                    .Update(v => v.Url($"{url}/Update/"))
                    .Destroy(v => v.Url($"{url}/Destroy/")));
            });

            return this;
        }

        public fxcLimitlessTreeList<T> Move(bool flag = true)
        {
            Editable(editable => editable.Move(flag));
            return this;
        }
    }

    public static class fxrLimitlessTreeList
    {
        public static fxcLimitlessTreeList<T> ToLimitlessTreeList<T>(
            this TreeListBuilder<T> @this)
            where T : class
        {
            var result = new fxcLimitlessTreeList<T>(@this.ToComponent());

            return result;
        }

        public static TreeListColumnBuilder<T> Bound<T, K>(
            this TreeListColumnFactory<T> @this,
            Expression<Func<T, K>> expression)
            where T : class
        {
            return @this.Add().Field(expression);
        }

        public static TreeListColumnBuilder<T> CommandEdit<T>(
            this TreeListColumnFactory<T> @this,
            bool edit = true,
            bool delete = true)
            where T : class
        {
            return @this.Add().Command(command =>
                {
                    if (edit) command.Edit();
                    if (delete) command.Destroy();
                })
                .Width(88);
        }
    }
}