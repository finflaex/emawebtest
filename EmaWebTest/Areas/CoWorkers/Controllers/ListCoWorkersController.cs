﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using EmaDbTest;
using EmaDbTest.Tables;
using EmaWebTest.Models;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Extensions;
using Finflaex.Support.MVCCore.Extensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace EmaWebTest.Areas.CoWorkers.Controllers
{
    [Area("CoWorkers")]
    [FatRoles("admin")]
    public class ListCoWorkersController : Controller
    {
        private readonly EmaDb _db;
        private IMemoryCache _cache;
        private AuthorizationModel auth => _cache.GetAuthModel<AuthorizationModel>(HttpContext);

        public ListCoWorkersController(
            EmaDb db,
            IMemoryCache cache
        )
        {
            _db = db;
            _cache = cache;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["deps"] = await _db
                .Departaments
                .Select(v => new
                {
                    v.ID,
                    v.Caption
                })
                .ToArrayAsync();
            return View();
        }

        public async Task<IActionResult> Read(
            [DataSourceRequest] DataSourceRequest request
        )
        {
            var result = await _db
                .CoWorkers
                .ToDataSourceResultAsync(request, read => read.AsType<GridModel>(true));

            return Json(result);
        }

        public async Task<IActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");
            var record = model.AsType<CoWorker>(true);
            model.ID = record.ID = Guid.NewGuid();
            await _db.AddAsync(record);
            await _db.CommitAsync(auth.UserId);
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .CoWorkers
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            record.CopyPropertyValuesFrom(model);
            await _db.CommitAsync(auth.UserId);
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .CoWorkers
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            _db.Remove(record);
            await _db.CommitAsync(auth.UserId);
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public class GridModel
            : FxiGuidTable
        {
            public Guid ID { get; set; }
            [DisplayName("Имя")] public string Name { get; set; }
            [DisplayName("Фамилия")] public string Surname { get; set; }
            [DisplayName("Возраст")] public int Old { get; set; }
            [DisplayName("Пол")] public CoWorker.MaleType Type { get; set; }
            [DisplayName("Отдел")] public Guid DepartamentGuid { get; set; }
        }

        public async Task<IActionResult> Departaments()
        {
            var read = await _db
                .Departaments
                .Select(v => new
                {
                    v.ID,
                    v.Caption
                })
                .ToArrayAsync();

            return Json(read);
        }
    }
}