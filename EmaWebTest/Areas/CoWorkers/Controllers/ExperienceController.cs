﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Threading.Tasks;
using EmaDbTest;
using EmaDbTest.Tables;
using EmaWebTest.Models;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Extensions;
using Finflaex.Support.Extensions;
using Finflaex.Support.MVCCore.Extensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace EmaWebTest.Areas.CoWorkers.Controllers
{
    [Area("CoWorkers")]
    [FatRoles("admin")]
    public class ExperienceController : Controller
    {
        private readonly EmaDb _db;
        private IMemoryCache _cache;
        private AuthorizationModel auth => _cache.GetAuthModel<AuthorizationModel>(HttpContext);

        public ExperienceController(
            EmaDb db,
            IMemoryCache cache
        )
        {
            _db = db;
            _cache = cache;
        }

        public async Task<IActionResult> Index(
            Guid? id
        )
        {
            ViewData["progs"] = await _db
                .ProgLanguages
                .Select(v => new
                {
                    v.ID,
                    v.Caption
                })
                .ToArrayAsync();
            id.ThrowIfNull("укажите ИД");
            return View(new ViewModel
            {
                ID = id.Value
            });
        }

        public async Task<IActionResult> Read(
            [DataSourceRequest] DataSourceRequest request
        )
        {
            var result = await _db
                .Experiences
                .ToDataSourceResultAsync(request, read => new GridModel
                {
                    ID = read.ID,
                    ProgLanguageGuid = read.ProgLanguageGuid,
                    Start = read.Start?.DateTime ?? DateTime.MinValue,
                    End = read.End?.DateTime ?? DateTime.MaxValue,
                });

            return Json(result);
        }

        public async Task<IActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");
            var record = new Experience
            {
                CoWorkerGuid = model.CoWorkerGuid,
                Start = model.Start,
                End = model.End,
                ProgLanguageGuid = model.ProgLanguageGuid
            };
            model.ID = record.ID;
            await _db.AddEntity(record).CommitAsync(auth.UserId);
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .Experiences
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            record.Start = model.Start;
            record.End = model.End;
            record.ProgLanguageGuid = model.ProgLanguageGuid;

            await _db.CommitAsync(auth.UserId);
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .Experiences
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            _db.Remove(record);
            await _db.CommitAsync(auth.UserId);
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public class GridModel
            : FxiGuidTable
        {
            public Guid ID { get; set; }
            public DateTime Start { get; set; }
            public DateTime End { get; set; }
            public Guid ProgLanguageGuid { get; set; }
            public Guid CoWorkerGuid { get; set; }
        }

        public class ViewModel
        {
            public Guid ID { get; set; }
        }

        public async Task<IActionResult> Departaments()
        {
            var read = await _db
                .Departaments
                .Select(v => new
                {
                    v.ID,
                    v.Caption
                })
                .ToArrayAsync();

            return Json(read);
        }
    }
}