﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using EmaDbTest;
using EmaDbTest.Tables;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Extensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;

namespace EmaWebTest.Areas.AdminUser.Controllers
{
    [Area("adminuser")]
    [FatRoles("admin")]
    public class UsersController : Controller
    {
        private readonly EmaDb _db;

        public UsersController(
            EmaDb db
        )
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Read(
            [DataSourceRequest] DataSourceRequest request
        )
        {
            var result = await _db
                .Users
                .ToDataSourceResultAsync(request, read =>
                {
                    return Activator
                        .CreateInstance<GridModel>()
                        .CopyPropertyValuesFrom(read);
                });

            return Json(result);
        }

        public async Task<IActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");
            var record = new UserTable().CopyPropertyValuesFrom(model);
            model.ID = record.ID = Guid.NewGuid();
            await _db.AddAsync(record);
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .Users
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            record.CopyPropertyValuesFrom(model);
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .Users
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            _db.Remove(record);
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public class GridModel
            : FxiGuidTable
                , FxiCaptionTable
        {
            [DisplayName("Логин")] public string Login { get; set; }
            [DisplayName("Название")] public string Caption { get; set; }
            public Guid ID { get; set; }
        }
    }
}