﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using EmaDbTest;
using EmaDbTest.Tables;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Extensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmaWebTest.Areas.AdminUser.Controllers
{
    [Area("adminuser")]
    public class UserRolesController : Controller
    {
        private readonly EmaDb _db;

        public UserRolesController(
            EmaDb db
        )
        {
            _db = db;
            var cultureInfo = new CultureInfo("ru-RU");
            cultureInfo.NumberFormat.NumberDecimalSeparator = ",";
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
        }

        public async Task<IActionResult> Index(
            Guid? id
        )
        {
            var model = await (
                    from root in _db.Users
                    where root.ID == id
                    select new ViewModel
                    {
                        Id = root.ID,
                        Caption = root.Caption
                    }
                )
                .FirstOrDefaultAsync()
                .ThrowIfNullAsync("Данные отсутствуют в базе");

            return View(model);
        }

        public async Task<IActionResult> Read(
            [DataSourceRequest] DataSourceRequest request,
            Guid? id
        )
        {
            var result = await (
                    from root in _db.Users
                    where root.ID == id
                    from data in root.Roles
                    select data
                )
                .ToDataSourceResultAsync(request, read =>
                {
                    var item = Activator
                        .CreateInstance<GridModel>()
                        .CopyPropertyValuesFrom(read);

                    item.StartDate = read.Start?.LocalDateTime;
                    item.EndDate = read.End?.LocalDateTime;

                    return item;
                });
            return Json(result);
        }

        public async Task<IActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");
            var record = new UserRoleLinkTable().CopyPropertyValuesFrom(model);
            record.Start = model.StartDate;
            record.End = model.EndDate;
            model.ID = record.ID = Guid.NewGuid();
            await _db.AddAsync(record);
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .UserRoleLinks
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            record.CopyPropertyValuesFrom(model);
            record.Start = model.StartDate;
            record.End = model.EndDate;
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .UserRoleLinks
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            _db.Remove(record);
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public class ViewModel
        {
            public Guid Id { get; set; }
            public string Caption { get; set; }
        }

        public class GridModel :
            FxiGuidTable
        {
            public GridModel()
            {
                StartDate = DateTime.Now;
                EndDate = DateTime.MaxValue;
            }

            [DisplayName("Начало")] public DateTime? StartDate { get; set; }
            [DisplayName("Окончание")] public DateTime? EndDate { get; set; }
            [DisplayName("Роль")] public Guid RoleGuid { get; set; }

            public Guid ID { get; set; }
        }
    }
}