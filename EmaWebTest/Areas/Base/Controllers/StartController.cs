﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EmaDbTest;
using EmaDbTest.Tables;
using EmaWebTest.Models;
using Finflaex.Support.Attributes;
using Finflaex.Support.Extensions;
using Finflaex.Support.MVCCore.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace EmaWebTest.Areas.Base.Controllers
{
    [Area("base")]
    public class StartController : Controller
    {
        private readonly IMemoryCache _cache;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly EmaDb _db;

        public StartController(
            IMemoryCache cache,
            EmaDb db,
            IHttpContextAccessor contextAccessor)
        {
            _cache = cache;
            _db = db;
            _contextAccessor = contextAccessor;
        }

        [FatRoles]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Login(LoginModel model)
        {
            if (model.Login.IsNullOrWhiteSpace() || model.Password.IsNullOrWhiteSpace())
            {
                model.Message = "введите логин и пароль";
                return View(model);
            }

            var user = await _db
                .Users
                .Where(v => v.Login == model.Login)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                model.Message = "логин неверный";
                return View(model);
            }

            var password = await _db
                .Entry(user)
                .Collection(v => v.Passwords)
                .Query()
                .Where(v => v.CreateDate < _db.CurrentTime)
                .FirstOrDefaultAsync(v => v.Type == EPasswordType.active);

            if (password?.Password != model.Password)
            {
                model.Message = "пароль неверный";
                return View(model);
            }

            await _db.CommitAsync();

            var roles = await _db
                .Entry(user)
                .Collection(v => v.Roles)
                .Query()
                .Where(v => v.Start < _db.CurrentTime)
                .Where(v => v.End > _db.CurrentTime)
                .Select(v => v.Role.Key)
                .ToArrayAsync();

            var auth = new AuthorizationModel
            {
                Caption = user.Caption,
                UserId = user.ID,
                Roles = roles
            };

            HttpContext.SetSession(auth.SessionId);
            _cache.SetModel(auth.SessionId, auth);

            return Redirect(model.Path ?? "/base/start/index");
        }


        public async Task<IActionResult> Logout()
        {
            var sessionGuid = HttpContext.GetSession();

            HttpContext.SetSession(null);
            _cache.SetModel<AuthorizationModel>(sessionGuid, null);

            return RedirectToAction("Index", "Start", new
            {
                area = "Base"
            });
        }

        [HttpPost]
        public void Control(ControlModel model)
        {
            switch (model.Type)
            {
                case "control":
                    Toggle(model);
                    break;
            }
        }

        private void Toggle(ControlModel model)
        {
            var session = HttpContext.GetSession();
            var auth = _cache.GetModel<AuthorizationModel>(session);
            if (auth == null) return;
            switch (model.Control)
            {
                case "main":
                    auth.ToggleMain = !auth.ToggleMain;
                    break;
                case "secondary":
                    auth.ToggleSecondary = !auth.ToggleSecondary;
                    break;
                case "opposite":
                    auth.ToggleOpposite = !auth.ToggleOpposite;
                    break;
            }
        }

        public class LoginModel
        {
            // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
            public string Login { get; set; } = string.Empty;

            // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
            public string Password { get; set; } = string.Empty;
            public string Message { get; set; } = string.Empty;
            public string Path { get; set; } = string.Empty;
        }

        public class ControlModel
        {
            public string Modul { get; set; } = string.Empty;

            // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
            public string Type { get; set; } = string.Empty;

            // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
            public string Control { get; set; } = string.Empty;
        }
    }
}