﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace EmaWebTest.Areas.Base.Controllers
{
    [Area("export")]
    public class ExportController : Controller
    {
        [HttpPost]
        public ActionResult Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }
    }
}