﻿using System.Threading.Tasks;
using EmaDbTest;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmaWebTest.Areas.DemoData.Controllers
{
    [Area("demodata")]
    public class ChartDataController : Controller
    {
        private readonly EmaDb _db;

        public ChartDataController(
            EmaDb db
        )
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Read()
        {
            var read = await _db.Demos.ToArrayAsync();
            return Json(read);
        }
    }
}