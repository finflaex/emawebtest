﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using EmaDbTest;
using EmaDbTest.Tables;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.Extensions;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmaWebTest.Areas.DemoData.Controllers
{
    [Area("demodata")]
    public class TableDataController : Controller
    {
        private readonly EmaDb _db;

        public TableDataController(
            EmaDb db
        )
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Read(
            [DataSourceRequest] DataSourceRequest request
        )
        {
            var result = await _db
                .Demos
                .ToDataSourceResultAsync(request, read =>
                {
                    var record = Activator
                        .CreateInstance<GridModel>()
                        .CopyPropertyValuesFrom(read);
                    record.DateTime = read.Date;
                    return record;
                });

            return Json(result);
        }

        public async Task<IActionResult> Create(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");
            var record = new DemoTable().CopyPropertyValuesFrom(model);
            model.ID = record.ID = Guid.NewGuid();
            record.Date = model.DateTime;
            await _db.AddAsync(record);
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Update(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .Demos
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            record.CopyPropertyValuesFrom(model);
            record.Date = model.DateTime;
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> Destroy(
            [DataSourceRequest] DataSourceRequest request,
            GridModel model
        )
        {
            model.ThrowIfNull("данные не указаны");

            var record = await _db
                .Demos
                .FindAsync(model.ID)
                .ThrowIfNullAsync("данные отсутствуют в базе");

            _db.Remove(record);
            await _db.CommitAsync();
            var result = await model.InArray().ToDataSourceResultAsync(request);
            return Json(result);
        }

        public async Task<IActionResult> ReadFilter(
            [DataSourceRequest] DataSourceRequest request,
            DateTime? start,
            DateTime? end,
            TimeSpan? startInterval,
            TimeSpan? endInterval
        )
        {
            var begindate = start ?? DateTime.MinValue;
            var enddate = end ?? DateTime.MaxValue;
            var begintime = startInterval ?? TimeSpan.Zero;
            var endtime = endInterval ?? TimeSpan.FromSeconds(86400);

            var list = await _db
                .Demos
                .Where(v => v.Date >= begindate)
                .Where(v => v.Date <= enddate)
                .Where(v => v.Date.TimeOfDay >= begintime)
                .Where(v => v.Date.TimeOfDay < endtime)
                .ToArrayAsync();

            var result = list
                .Select(read =>
                {
                    var record = Activator
                        .CreateInstance<GridModel>()
                        .CopyPropertyValuesFrom(read);

                    record.DateTime = read.Date;
                    record.DateString = $"{read.Date:yyyy.MM.dd}";
                    record.TimeString = $"{read.Date:HH:mm:ss}";

                    return record;
                })
                .ToDataSourceResult(request);

            return Json(result);
        }

        public class GridModel
            : FxiGuidTable
        {
            [DisplayName("Дата и время")] public DateTime DateTime { get; set; }
            [DisplayName("Дата")] public string DateString { get; set; }
            [DisplayName("Время")] public string TimeString { get; set; }
            [DisplayName("X переменнаяя")] public double X { get; set; }
            [DisplayName("Y переменнаяя")] public double Y { get; set; }
            [DisplayName("Z переменнаяя")] public double Z { get; set; }
            public Guid ID { get; set; }
        }
    }
}