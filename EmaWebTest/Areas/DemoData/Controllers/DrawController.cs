﻿using Microsoft.AspNetCore.Mvc;

namespace EmaWebTest.Areas.DemoData.Controllers
{
    [Area("DemoData")]
    public class DrawController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}