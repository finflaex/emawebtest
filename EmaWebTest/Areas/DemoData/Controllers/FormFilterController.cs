﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EmaDbTest;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmaWebTest.Areas.DemoData.Controllers
{
    [Area("DemoData")]
    public class FormFilterController : Controller
    {
        private readonly EmaDb _db;

        public FormFilterController(EmaDb db)
        {
            _db = db;
        }

        public async Task<IActionResult> Index()
        {
            var model = await (
                    from demo in _db.Demos
                    let min = _db.Demos.Min(v => v.Date)
                    let max = _db.Demos.Max(v => v.Date)
                    select new ViewModel
                    {
                        start = min,
                        end = max
                    }
                )
                .FirstAsync();
            return View(model);
        }

        public class ViewModel
        {
            public DateTime start { get; set; }
            public DateTime end { get; set; }
        }
    }
}