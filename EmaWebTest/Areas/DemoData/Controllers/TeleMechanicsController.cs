﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EmaWebTest.Areas.DemoData.Controllers
{
    public class TeleMechanicsController : Controller
    {
        [Area("DemoData")]
        public IActionResult Index()
        {
            return View();
        }
    }
}