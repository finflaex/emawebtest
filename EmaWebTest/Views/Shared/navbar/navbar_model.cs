﻿using EmaWebTest.Models;

namespace EmaWebTest.Views.Shared.navbar
{
    public class navbar_model
    {
        public string Caption = "EmaTest";
        public AuthorizationModel Auth { get; set; }
    }
}