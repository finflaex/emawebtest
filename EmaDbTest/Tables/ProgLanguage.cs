﻿using System;
using System.Collections.Generic;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace EmaDbTest.Tables
{
    public class ProgLanguage
        : FxaGuidTable
            , FxiCaptionTable
            , FxiCreateAuthorTable
            , FxiCreateDateTable
            , FxiUpdateAuthorTable
            , FxiUpdateDateTable
    {
        public string Caption { get; set; }

        public virtual ICollection<Experience> Experiences { get; set; }
            = new HashSet<Experience>();

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProgLanguage>()
                .HasMany(v => v.Experiences).WithOne(v => v.ProgLanguage)
                .HasForeignKey(v => v.ProgLanguageGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }

        public Guid? CreateAuthorID { get; set; }
        public Guid? UpdateAuthorID { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public DateTimeOffset? UpdateDate { get; set; }
    }
}