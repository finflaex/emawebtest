﻿using System;
using System.Collections.Generic;
using System.Text;
using Finflaex.Support.EFCore.Tables.Abstracts;

namespace EmaDbTest.Tables
{
    public class DemoTable
    : FxaGuidTable
    {
        public DateTime Date { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}
