﻿using System;
using System.Collections.Generic;
using System.Text;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;

namespace EmaDbTest.Tables
{
    public class UserTable
        : FxaGuidTable
            , FxiCaptionTable
            , FxiCreateDateTable
    {
        public string Caption { get; set; }
        public string Login { get; set; }
        public virtual ICollection<UserRoleLinkTable> Roles { get; set; } = new HashSet<UserRoleLinkTable>();
        public virtual ICollection<PasswordTable> Passwords { get; set; } = new HashSet<PasswordTable>();
        public DateTimeOffset? CreateDate { get; set; }
    }
}