﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;
using Finflaex.Support.Extensions;

namespace EmaDbTest.Tables
{
    public enum EPasswordType
    {
        blocked = -1,
        unknown = 0,
        active = 1,
    }

    public class PasswordTable
        : FxaGuidTable
            , FxiCreateAuthorTable
            , FxiCreateDateTable
            , FxiTypeTable<EPasswordType>
    {
        public Guid? CreateAuthorID { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public EPasswordType Type { get; set; }
        public Guid UserGuid { get; set; }
        public virtual UserTable User { get; set; }

        [Column("Password")] public byte[] RawPassword { get; set; }

        [NotMapped]
        public string Password
        {
            get => RawPassword.Decrypt().AsString();
            set => RawPassword = value.AsBytes().Encrypt();
        }
    }
}