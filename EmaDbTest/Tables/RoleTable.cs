﻿using System;
using System.Collections.Generic;
using System.Text;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;

namespace EmaDbTest.Tables
{
    public class RoleTable
        : FxaGuidTable
            , FxiCaptionTable
            , FxiKeyTable
    {
        public string Caption { get; set; }
        public string Key { get; set; }

        public virtual ICollection<UserRoleLinkTable> Users { get; set; } = new HashSet<UserRoleLinkTable>();
    }
}