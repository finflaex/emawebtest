﻿using System;
using System.Collections.Generic;
using System.Text;
using Finflaex.Support.Attributes;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace EmaDbTest.Tables
{
    public class CoWorker
        : FxaGuidTable
            , FxiCreateAuthorTable
            , FxiCreateDateTable
            , FxiUpdateAuthorTable
            , FxiUpdateDateTable
            , FxiTypeTable<CoWorker.MaleType>
    {
        public enum MaleType
        {
            [FatValue("неизвестно")] unknown = 0,

            [FatValue("мужчина")] male = 1,

            [FatValue("женщина")] female = 2,

            [FatValue("оно")] neuter = 3,

            [FatValue("была мужчиной")] became_a_man = 4,

            [FatValue("был женщиной")] became_a_woman = 5,
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public int Old { get; set; }
        public MaleType Type { get; set; }
        public Guid DepartamentGuid { get; set; }
        public virtual Departament Departament { get; set; }

        public virtual ICollection<Experience> Experiences { get; set; }
            = new HashSet<Experience>();

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CoWorker>()
                .HasMany(v => v.Experiences).WithOne(v => v.CoWorker)
                .HasForeignKey(v => v.CoWorkerGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }

        public Guid? CreateAuthorID { get; set; }
        public Guid? UpdateAuthorID { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public DateTimeOffset? UpdateDate { get; set; }
    }
}