﻿using System;
using System.Collections.Generic;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace EmaDbTest.Tables
{
    public class Departament
        : FxaGuidTable
            , FxiCaptionTable
            , FxiCreateAuthorTable
            , FxiCreateDateTable
            , FxiUpdateAuthorTable
            , FxiUpdateDateTable
    {
        public string Caption { get; set; }
        public int Flor { get; set; }

        public virtual ICollection<CoWorker> CoWorkers { get; set; }
            = new HashSet<CoWorker>();

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Departament>()
                .HasMany(v => v.CoWorkers).WithOne(v => v.Departament)
                .HasForeignKey(v => v.DepartamentGuid).IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }

        public Guid? CreateAuthorID { get; set; }
        public Guid? UpdateAuthorID { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public DateTimeOffset? UpdateDate { get; set; }
    }
}