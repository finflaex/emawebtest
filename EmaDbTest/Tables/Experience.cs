﻿using System;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;
using Microsoft.EntityFrameworkCore;

namespace EmaDbTest.Tables
{
    public class Experience
        : FxaGuidTable
            , FxiCreateAuthorTable
            , FxiCreateDateTable
            , FxiUpdateAuthorTable
            , FxiUpdateDateTable
            , FxiStartEndTable
    {
        public Guid CoWorkerGuid { get; set; }
        public virtual CoWorker CoWorker { get; set; }
        public Guid ProgLanguageGuid { get; set; }
        public virtual ProgLanguage ProgLanguage { get; set; }

        public static void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        public Guid? CreateAuthorID { get; set; }
        public Guid? UpdateAuthorID { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public DateTimeOffset? UpdateDate { get; set; }
    }
}