﻿using System;
using System.Collections.Generic;
using System.Text;
using Finflaex.Support.DB.Tables.Interfaces;
using Finflaex.Support.EFCore.Tables.Abstracts;

namespace EmaDbTest.Tables
{
    public class UserRoleLinkTable
        : FxaGuidTable
            , FxiCreateAuthorTable
            , FxiCreateDateTable
            , FxiUpdateAuthorTable
            , FxiUpdateDateTable
            , FxiStartEndTable
    {
        public Guid? CreateAuthorID { get; set; }
        public Guid? UpdateAuthorID { get; set; }

        public Guid UserGuid { get; set; }
        public virtual UserTable User { get; set; }
        public Guid RoleGuid { get; set; }
        public virtual RoleTable Role { get; set; }
        public DateTimeOffset? Start { get; set; }
        public DateTimeOffset? End { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public DateTimeOffset? UpdateDate { get; set; }
    }
}