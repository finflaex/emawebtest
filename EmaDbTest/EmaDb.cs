﻿using System;
using System.Linq;
using EmaDbTest.Tables;
using Finflaex.Support.EFCore.Context;
using Finflaex.Support.Extensions;
using Microsoft.EntityFrameworkCore;

namespace EmaDbTest
{
    public class EmaDb : FxaDbContext<EmaDb>
    {
        public EmaDb()
        {
            var systemGuid = 1.ToGuid();

            var read = Users.Any(v => v.ID == systemGuid);

            if (!read)
            {
                Add(new UserTable
                {
                    ID = systemGuid,
                    Caption = "Система"
                });

                Add(new UserTable
                {
                    Caption = "Демо пользователь",
                    Login = "demo",
                    Passwords = new PasswordTable
                    {
                        Type = EPasswordType.active,
                        Password = "demo"
                    }
                        .InArray()
                });

                Add(new UserTable
                {
                    Caption = "Администратор",
                    Login = "admin",
                    Passwords = new PasswordTable
                    {
                        Type = EPasswordType.active,
                        Password = "admin"
                    }
                        .InArray(),
                    Roles = new UserRoleLinkTable
                    {
                        Role = new RoleTable
                        {
                            Key = "admin",
                            Caption = "роль администратора"
                        }
                    }
                        .InArray()
                });

                Commit();

                var random = new Random();
                var any = Demos.Any();
                var date = any
                    ? Demos.Max(v => v.Date)
                    : new DateTime(2018, 1, 1, 1, 0, 0);

                for (var i = 0; i < 10; i++)
                {
                    date = date.AddMinutes(90);

                    Add(new DemoTable
                    {
                        Date = date,
                        X = random.Next(101),
                        Y = random.Next(101),
                        Z = random.Next(101)
                    });

                    Commit(systemGuid);
                }

                ProgLanguages.AddRange(
                    new ProgLanguage
                    {
                        Caption = "c#",
                    },
                    new ProgLanguage
                    {
                        Caption = "c++",
                    },
                    new ProgLanguage
                    {
                        Caption = "java",
                    }
                );

                Departaments.AddRange(
                    new Departament
                    {
                        Caption = "отдел 1",
                        Flor = 1,
                    },
                    new Departament
                    {
                        Caption = "отдел 2",
                        Flor = 2,
                    },
                    new Departament
                    {
                        Caption = "отдел 3",
                        Flor = 3,
                    }
                );

                Commit(systemGuid);
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            }
        }

        public DbSet<UserTable> Users { get; set; }
        public DbSet<RoleTable> Roles { get; set; }
        public DbSet<PasswordTable> Passwords { get; set; }
        public DbSet<UserRoleLinkTable> UserRoleLinks { get; set; }
        public DbSet<DemoTable> Demos { get; set; }

        public DbSet<CoWorker> CoWorkers { get; set; }
        public DbSet<Departament> Departaments { get; set; }
        public DbSet<ProgLanguage> ProgLanguages { get; set; }
        public DbSet<Experience> Experiences { get; set; }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<RoleTable>().HasMany(v => v.Users)
                .WithOne(v => v.Role).HasForeignKey(v => v.RoleGuid)
                .OnDelete(DeleteBehavior.Cascade);

            mb.Entity<UserTable>().HasMany(v => v.Passwords)
                .WithOne(v => v.User).HasForeignKey(v => v.UserGuid)
                .OnDelete(DeleteBehavior.Cascade);

            mb.Entity<UserTable>().HasMany(v => v.Roles)
                .WithOne(v => v.User).HasForeignKey(v => v.UserGuid)
                .OnDelete(DeleteBehavior.Cascade);

            CoWorker.OnModelCreating(mb);
            Departament.OnModelCreating(mb);
            ProgLanguage.OnModelCreating(mb);
            Experience.OnModelCreating(mb);
        }
    }
}