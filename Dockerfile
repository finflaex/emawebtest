FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 45396
EXPOSE 44333

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY EmaWebTest/EmaWebTest.csproj EmaWebTest/
RUN dotnet restore EmaWebTest/EmaWebTest.csproj
COPY . .
WORKDIR /src/EmaWebTest
RUN dotnet build EmaWebTest.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish EmaWebTest.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "EmaWebTest.dll"]



#FROM microsoft/dotnet:2.1-aspnetcore-runtime
#FROM microsoft/dotnet:2.1-sdk
#COPY . /app
#WORKDIR /app/EmaWebTest
#RUN ["dotnet", "build"]
#
#EXPOSE 80/tcp
#ENV ASPNETCORE_URLS http://*:80
#
#FROM build AS publish
#RUN dotnet publish EmaWebTest.csproj -c Release -o /app
#
#WORKDIR /app
#COPY --from=publish /app .
#ENTRYPOINT ["dotnet", "EmaWebTest.dll"]
